$(document).ready(function(){
    $(".fancy-photo").fancybox({
    	openEffect	: 'elastic',
    	closeEffect	: 'elastic'
    });

    $('.justified').justifiedGallery({
	    rowHeight : 180,
    	lastRow : 'nojustify'
    });

    /// Tabs
    $('.tabs .tab').on('click', function(e){
    	e.preventDefault();
    	$('.tabs .tab').removeClass('active');
    	$(this).addClass('active');
    	var tgt = "#tab-" + $(this).data('tab');
    	$('.tabs .tab-content').slideUp(300);
    	$(tgt).slideDown(700);
    });

    // Header Search
    $('body').not('#header-search input').on('click', function(){
        $('#header-search .dropdown').toggleClass('visible');
    });

    $('#header-search input').on('focus', function(){
        $('#header-search .dropdown').toggleClass('visible');
    });



})